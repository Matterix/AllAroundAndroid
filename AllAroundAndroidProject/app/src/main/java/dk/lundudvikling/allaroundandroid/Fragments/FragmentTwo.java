package dk.lundudvikling.allaroundandroid.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import dk.lundudvikling.allaroundandroid.R;

public class FragmentTwo extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Maintenance ting, imens view bliver oprettet
        View rootView = inflater.inflate(R.layout.fragment_two, container,false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //Når view er blevet oprettet og man kan arbejde med elementer
        super.onViewCreated(view, savedInstanceState);
    }
}
