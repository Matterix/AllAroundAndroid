package dk.lundudvikling.allaroundandroid;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import dk.lundudvikling.allaroundandroid.Fragments.FragmentOne;
import dk.lundudvikling.allaroundandroid.Fragments.FragmentTwo;
import dk.lundudvikling.allaroundandroid.Handlers.FragmentHandler;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private boolean isDrawerOpen;
    private FragmentHandler mFragmentHandler;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.fabDrawer) FloatingActionButton mFabDrawer;
    @BindView(R.id.mainView) FrameLayout mMainView;
    @BindView(R.id.nav_view) NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initializeNavigationDrawer();
        mFragmentHandler = new FragmentHandler(this);
    }

    private void initializeNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout.addDrawerListener(new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                isDrawerOpen = false;
                supportInvalidateOptionsMenu();
            }
            public void onDrawerOpened(View drawerView) {
                isDrawerOpen = true;
                supportInvalidateOptionsMenu();
            }
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mMainView.setTranslationX(slideOffset * drawerView.getWidth());
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mMainView.setZ(2);
                    mFabDrawer.setZ(1);
                }
                drawerAnimation(slideOffset);
            }
        });

        mFabDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            //Fragment skifter side her
            case R.id.nav_1:
                mFragmentHandler.startTransactionNoAnimationWithBackstack(new FragmentOne());
                Toast.makeText(this, "Første side", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_2:
                mFragmentHandler.startTransactionNoAnimationWithBackstack(new FragmentTwo());
                Toast.makeText(this, "Anden side", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_3:
                Toast.makeText(this, "Tredje side", Toast.LENGTH_SHORT).show();
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void drawerAnimation(final float slideOffset) {
        final DrawerArrowDrawable drawable = new DrawerArrowDrawable(this);
        mFabDrawer.setColorFilter(Color.WHITE);
        mFabDrawer.setImageDrawable(drawable);

        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                drawable.setProgress(slideOffset);
            }
        });
        if (isDrawerOpen) {
            animator.start();
        } else {
            animator.reverse();
        }
    }
}
